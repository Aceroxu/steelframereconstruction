﻿#define Controller

using UnityEngine;
using UnityEngine.Networking;
using System.Collections;



public class PlayerSetup : NetworkBehaviour
{
#if !Controller
    public GameObject cam;

#else
    public Camera chimera;
    public CameraController camCon;
#endif
    void Start ()
    {
        if (isLocalPlayer)
        {
#if !Controller
            cam.SetActive(true);
#else
            chimera.enabled = true;
            camCon.enabled = true;
#endif
        }
	}
}
