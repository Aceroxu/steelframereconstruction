﻿//#define TEST
//#define DISTANCE_CHECK
#define COOLDOWN

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnPoints : MonoBehaviour {

    public bool isOccupied = false;

    #if DISTANCE_CHECK
    public float minDistance = 2;
    public bool checkListOnly = false;
    public List<GameObject> listToCheck;
    #endif

    #if COOLDOWN
        public float isOccupiedTimeout = 5;
    #endif

    void Awake()
    {
        #if DISTANCE_CHECK
            listToCheck = new List<GameObject>();
        #endif
    }

    public void Spawn(GameObject go)
    {
        GameObject.Instantiate(go, transform.position, transform.rotation);
        isOccupied = true;

        #if COOLDOWN
        StartCoroutine("IsOccupiedTimeout");
        #endif
    }

    public bool IsOccupied()
    {
		#if DISTANCE_CHECK
        isOccupied = false;

		if (checkListOnly)
		{
			for (int i = 0; i < listToCheck.Count; i++)
			{
				float dist;
				dist = Vector3.Distance(transform.position,
						listToCheck[i].transform.position);

				if (dist <= minDistance)
				{
					isOccupied = true;
					return isOccupied;
				}
			}
		}
		else
		{
			GameObject[] goList = FindObjectsOfType<GameObject>();
			for (int i = 0; i < goList.Length; i++)
			{
				float dist;
				dist = Vector3.Distance(transform.position,
						goList[i].transform.position);

				if (dist <= minDistance)
				{
					isOccupied = true;
					return isOccupied;
				}
			}
		}
        #endif

        return isOccupied; 
    }

    #if COOLDOWN
    IEnumerator IsOccupiedTimeout()
    {
        yield return new WaitForSeconds(isOccupiedTimeout);
        #if TEST
        print("No longer Occupied");
        #endif
        isOccupied = false;
    }
    #endif

}
