﻿using UnityEngine;
using System.Collections;

public class MinimapFollowTarget : MonoBehaviour
{
	public Transform target;
	
	void Start ()
	{
		if (!target)
			Destroy(this);
	}
	
	void Update ()
	{
		Vector3 pos = new Vector3(target.position.x, transform.position.y, target.position.z);
		transform.position = pos;
	}
}
