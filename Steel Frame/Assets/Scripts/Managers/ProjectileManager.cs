﻿using UnityEngine;
using System.Collections;

public class ProjectileManager : MonoBehaviour
{
    public static ProjectileManager PM;

    public GameObject laserPrefab;
    GameObject[] laserPool;

    const int poolSize = 128;
    int inuse = 0;

    // Use this for initialization
    void Awake ()
    {
        if (PM == null)
            PM = this;


        laserPool = new GameObject[poolSize];

        for(int i = 0; i < poolSize; i++)
        {
            laserPool[i] = (GameObject)Instantiate(laserPrefab, new Vector3(-1000, -1000, -1000), Quaternion.identity);
            //laserPool[i].GetComponent<LaserProjectile>().id = i;
            laserPool[i].SetActive(false);
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
	    
	}

    /*
    public void GetLaser(Vector3 position, Quaternion rotation)
    {
        laserPool[inuse].gameObject.SetActive(true);

        laserPool[inuse].transform.position = position;
        laserPool[inuse].transform.rotation = rotation;

        inuse++;
    }

    public void ClearLaser(GameObject laser)
    {
        laser.transform.position = new Vector3(-1000, -1000, -1000);

        laser.gameObject.SetActive(false);
        inuse--;

        //int b1 = laser.GetComponent<LaserProjectile>().id;
        int b2 = inuse;

        //Swap(ref laserPool[b1].GetComponent<LaserProjectile>().id, ref laserPool[b2].GetComponent<LaserProjectile>().id);

        GameObject temp = laserPool[b1];

        laserPool[b1] = laserPool[b2];
        laserPool[b2] = temp;
    }
    */

    void Swap<T>(ref T lhs, ref T rhs)
    {
        T temp;
        temp = lhs;
        lhs = rhs;
        rhs = temp;
    }

    /*
    public int MAXBULLETS = 256;
    public GameObject bullet_prefab;

    int inuse = 0; // Number of bullets in use
    public List<Bullet> pool = new List<Bullet>();

    // Use this for initialization
    void Awake()
    {
        for (int i = 0; i < MAXBULLETS; i++)
        {
            GameObject go = (GameObject)Instantiate(bullet_prefab);
            pool.Add(go.GetComponent<Bullet>());

            go.name = "bullet_" + i.ToString("000");

            go.transform.parent = transform;
            go.SetActive(false);
        }
    }

    void Swap<T>(ref T lhs, ref T rhs)
    {
        T temp;
        temp = lhs;
        lhs = rhs;
        rhs = temp;
    }

    public Bullet GetBullet()
    {
        pool[inuse].gameObject.SetActive(true);

        return pool[inuse++];
    }

    public void ClearBullet(Bullet bill)
    {
        bill.gameObject.SetActive(false);
        inuse--;

        int b1 = bill.id;
        int b2 = inuse;

        Swap(ref pool[b1].id, ref pool[b2].id);

        Bullet temp = pool[b1];

        pool[b1] = pool[b2];
        pool[b2] = temp;

        // Swap (ref pool[b1], ref pool[b2]);
    }
    */
}