﻿using UnityEngine;
using System.Collections;

public class GUIManager : MonoBehaviour
{
	public GameObject WeaponPanel;
	public GameObject HealthPanel;
	public GameObject MotionPanel;
	
	public float maxSpeed = 5.0f;
	public float maxReverseSpeed = 5.0f;
	
	GameObject mech;
	Laser mechWeapon1;
	Laser mechWeapon2;
	
	GameObject PosSpeedMeter;
	GameObject NegSpeedMeter;
	GameObject JumpJetMeter;
	
	Material PosSpeedMat;
	Material NegSpeedMat;
	Material JumpJetMat;
	
	Material Weapon1Reload;
	Material Weapon2Reload;
	
	UnityEngine.UI.Text SpeedText;
	
	void Start ()
	{
		if (MotionPanel)
		{
			//Find and connect the panels of the GUI
			JumpJetMeter = MotionPanel.transform.FindChild("JumpJetFuel").gameObject;
			PosSpeedMeter = MotionPanel.transform.FindChild("SpeedPanel/PositiveMeter").gameObject;
			NegSpeedMeter = MotionPanel.transform.FindChild("SpeedPanel/NegativeMeter").gameObject;
			SpeedText = MotionPanel.transform.FindChild("SpeedPanel/Speed").GetComponent<UnityEngine.UI.Text>();
			
			//Connect the meter materials
			PosSpeedMat = PosSpeedMeter.GetComponent<UnityEngine.UI.Image>().material;
			NegSpeedMat = NegSpeedMeter.GetComponent<UnityEngine.UI.Image>().material;
			JumpJetMat = JumpJetMeter.GetComponent<UnityEngine.UI.Image>().material;
			
			Weapon1Reload = WeaponPanel.transform.FindChild("WeaponPanel1/ReloadBar").GetComponent<UnityEngine.UI.Image>().material;
			Weapon2Reload = WeaponPanel.transform.FindChild("WeaponPanel2/ReloadBar").GetComponent<UnityEngine.UI.Image>().material;
			
			//Initialize meters
			PosSpeedMat.SetFloat("_FillAmount", 0.0f);
			NegSpeedMat.SetFloat("_FillAmount", 0.0f);
			JumpJetMat.SetFloat("_FillAmount", 1.0f);
			SpeedText.text = "0" + " km/h";
		}
	}
	
	void Update()
	{
		if (mech)
		{
			//Get Speed & Jump Jets Values
			PlayerController p = mech.GetComponent<PlayerController>();
			
			float vert = p.GetVertical();
			if (vert < 0)
				vert = -maxReverseSpeed;
			else if (vert > 0)
				vert = maxSpeed;
			SetSpeed(vert);	
			
			float percent = p.GetFuel() / p.GetMaxFuel();
			SetJumpJet(percent);
		}
		if (mechWeapon1)
		{
			//Get Weapon Reload Times
			float percent = mechWeapon1.getCurrentHeat() / mechWeapon1.getMaxHeat ();
			SetWeaponReload(1,percent);
		}
		if (mechWeapon2)
		{
			//Get Weapon Reload Times
			float percent = mechWeapon2.getCurrentHeat() / mechWeapon2.getMaxHeat ();
			SetWeaponReload(2,percent);
		}
	}
	
	public void SetMech(GameObject mObject)
	{
		mech = mObject;
	}
	
	public void SetMechWeapons(Laser l1, Laser l2)
	{
		mechWeapon1 = l1;
		mechWeapon2 = l2;
	}
	
	//Takes a value for speed and adjusts the speed meter to correspond to that amount
	//MaxSpeed is full forward speed bar
	//MaxReverseSpeed is full reverse speed bar
	//0 is both bars empty
	public void SetSpeed(float speed)
	{
		if (speed == 0.0f)
		{
			PosSpeedMat.SetFloat("_FillAmount", 0.0f);
			NegSpeedMat.SetFloat("_FillAmount", 0.0f);
		}
		else if (speed > 0)
		{
			NegSpeedMat.SetFloat("_FillAmount", 0.0f);
			float val = speed / maxSpeed;
			PosSpeedMat.SetFloat("_FillAmount", val);
		}
		else if (speed < 0)
		{
			PosSpeedMat.SetFloat("_FillAmount", 0.0f);
			float val = -speed / maxReverseSpeed;
			NegSpeedMat.SetFloat("_FillAmount", val);
		}
		SpeedText.text = "" + speed + " km/h";
	}
	
	//Takes a value between 0 and 100 and adjusts the meter to correspond to the amount
	//0 is empty, no meter filled
	//100 is full, meter completely filled
	public void SetJumpJet(float fuel)
	{
		JumpJetMat.SetFloat("_FillAmount", fuel);
	}
	//Takes a weapon number between 1 and 2
	//Takes a float between 0 and 1
	//Sets reload bar display to percent for weapon number
	public void SetWeaponReload(int weaponNum, float percent)
	{
		switch(weaponNum)
		{
		case 1:
			Weapon1Reload.SetFloat("_FillAmount", percent);
			break;
		case 2:
			Weapon2Reload.SetFloat("_FillAmount", percent);
			break;
		};
	}
}