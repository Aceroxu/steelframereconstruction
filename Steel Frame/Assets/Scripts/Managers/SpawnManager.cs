﻿#define TEST

using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

public class SpawnManager : NetworkBehaviour
{
	private static SpawnManager singleton;
	public GameObject projectilePrefab;
	public float projectileLifetime;
    public SpawnPoints[] spawnPoints;

    //Shuffled list of spawn point indices
    public List<int> spawnPointOrder;
    public bool shuffleSpawnOrder = true;

    #if TEST
        public bool testShuffle;
        public bool testSpawn;
    #endif
	
	public static SpawnManager instance
	{
		get
		{
			if (singleton == null)
			{
				singleton = GameObject.FindObjectOfType<SpawnManager>();
				if (singleton == null)
				{
					GameObject managers = GameObject.Find("Managers");
					if (managers == null)
					{
						managers = new GameObject("Managers");
						singleton = managers.AddComponent<SpawnManager>();
					}
				}
			}
			return singleton;
		}
	}

    void Awake()
    {
        spawnPoints = GameObject.FindObjectsOfType<SpawnPoints>();

        spawnPointOrder = new List<int>();
        for (int i = 0; i < spawnPoints.Length; i++)
        {
            spawnPointOrder.Add(i);
        }
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        #if TEST
            TestShuffle();
            TestSpawn();
        #endif

    }

    public void Spawn(GameObject go)
    {
        if (shuffleSpawnOrder)
            Shuffle(spawnPointOrder);

        for (int i = 0; i < spawnPoints.Length; i++)
        {
            if (spawnPoints[spawnPointOrder[i]].IsOccupied() == false)
            {
                spawnPoints[spawnPointOrder[i]].Spawn(go);
                return;
            }
        }
        spawnPoints[Random.Range(0, spawnPoints.Length)].Spawn(go);
    }

    // Shuffle spawns to make spawn location unpredictable to campers
    void Shuffle(List<int> list)
    {
        for (int i = 0; i < spawnPoints.Length; i++)
        {
            // Stores the int to be swapped
            int swap = spawnPointOrder[i];
            // Stores the index of the swap destination
            int swap_target = Random.Range(0, spawnPoints.Length);

            spawnPointOrder[i] = spawnPointOrder[swap_target];
            spawnPointOrder[swap_target] = swap;
        }
    }

    #if TEST
    void TestShuffle()
    {
        if (testShuffle)
        {
            Shuffle(spawnPointOrder);
            testShuffle = false;
        }
    }

    void TestSpawn()
    {
        if (testSpawn)
        {
            GameObject go = new GameObject();
            go.AddComponent<Light>();
            go.AddComponent<SphereCollider>();
            Spawn(go);
            testSpawn = false;
        }
    }
    #endif
	
}
