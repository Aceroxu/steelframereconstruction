﻿Shader "Custom/TextureMasking"
{
	Properties
	{
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Mask ("Texture Mask", 2D) = "white" {}
		_Cutoff ("Alpha Cutoff", Range(0,1)) = 0.1
	}
	SubShader
	{
		Tags {"Queue"="Transparent"}
    	Lighting Off
    	ZWrite Off
    	Blend SrcAlpha OneMinusSrcAlpha
    	AlphaTest GEqual [_Cutoff]
    	Pass
    	{
        	SetTexture [_Mask] {combine texture}
        	SetTexture [_MainTex] {combine texture, previous}
    	}
	} 
	FallBack "Diffuse"
}
