﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    public Camera camera;
	public Transform target;
	public float pitch;
	public float sensitivity;

	private Vector3 rotation;
	private float input;
    private int index;

	public void LateUpdate()
	{
		SwitchViews();
		Inputs();
	}

	public void SwitchViews()
	{
		if (target)
		{
            camera.transform.position = target.position;
            camera.transform.rotation = target.rotation;
		}
	}

	public void Inputs()
	{
		input -= sensitivity * Input.GetAxis("Mouse Y");
		rotation = Mathf.Clamp(input, -pitch, pitch) * Vector3.right;
        camera.transform.Rotate(rotation);
	}
}
