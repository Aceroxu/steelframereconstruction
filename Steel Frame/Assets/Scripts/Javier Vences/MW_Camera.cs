﻿using UnityEngine;
using System.Collections;

public class MW_Camera : MonoBehaviour 
{
	public bool m_useBadAngles = false;
	public bool m_mouseVisible = true;

	public float m_vertSensitivity = 5;
	public float m_maxVert = 30;
	public float m_minVert = -30;

	public float m_horSensitivity = 5;
	public float m_maxHor = 90;
	public float m_minHor = -90;

	private float m_x = 0.0f;
	private float m_y = 0.0f;
	private float m_z = 0.0f;

	private bool m_centering = false;

	void Start()
	{
		m_x = transform.localEulerAngles.x;
		m_y = transform.localEulerAngles.y;
		m_z = transform.localEulerAngles.z;
	}

	void Update()
	{
		if (!m_centering) 
		{
			if(Input.GetButtonDown("CenterCamera"))
				StartCoroutine(CenterCamera());

			Cursor.visible = m_mouseVisible;

			RotateHorizontal ();
			RotateVertical ();

			Quaternion rot = Quaternion.Euler (GetEuler ());
			transform.localRotation = Quaternion.Slerp (transform.localRotation, rot, Time.time);
		}
	}

	void RotateHorizontal()
	{
		float horizontal = Input.GetAxis ("Mouse X");

		m_y = Mathf.Clamp (m_y + (horizontal * m_horSensitivity * Time.deltaTime), m_minHor, m_maxHor);
	}

	void RotateVertical()
	{
		float vertical = Input.GetAxis ("Mouse Y");

		m_x = Mathf.Clamp (m_x - (vertical * m_vertSensitivity * Time.deltaTime), m_minVert, m_maxVert);
	}

	Vector3 GetEuler()
	{
		float angleX = m_x;
		if (m_x < 0)
			angleX = 360.0f + m_x;

		float angleY = m_y;
		if (m_y < 0)
			angleY = 360.0f + m_y;

		Vector3 modEuler = new Vector3(angleX, angleY, m_z);

		if (m_useBadAngles)
            modEuler = new Vector3(-angleY, -m_z, -angleX);
	
		return modEuler;
	}

	IEnumerator CenterCamera()
	{
		m_centering = true;

		float elapsedTime = 0.0f;
		Quaternion startRot = transform.localRotation;

		while (m_centering) {
			yield return null;

			elapsedTime = Mathf.Clamp(elapsedTime + Time.deltaTime, 0.0f, 1.0f);

			transform.localRotation = Quaternion.Slerp(startRot, Quaternion.identity, elapsedTime);

			if(transform.localRotation == Quaternion.identity)
			{
				m_centering = false;
				m_x = transform.localEulerAngles.x;
				m_y = transform.localEulerAngles.y;
				
				
			}

			/*test to stop rotation mid way
			float vertical = Input.GetAxis ("Mouse Y");
			float horizontal = Input.GetAxis ("Mouse X");

			if((transform.localRotation == Quaternion.identity) || (vertical != 0.0f) || (horizontal != 0.0f))
			{
				m_centering = false;
				m_x = transform.localEulerAngles.x;
				m_y = transform.localEulerAngles.y;


			}*/
		}
	}
}
