﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PlayerIdentity : NetworkBehaviour
{
	[SyncVar] public string playerIdentity;

	public NetworkInstanceId playerNetID;
	public Transform playerTransform;

	public override void OnStartLocalPlayer()
	{
		GetNetworkIdentity();
		SetIdentity();
		//base.OnStartLocalPlayer();
	}

	public void Update()
	{
		if (playerTransform.name != playerIdentity)
			SetIdentity();
	}

	public string MakeUniqueIdentity()
	{
		string identity = "Player " + playerNetID.ToString();
		return identity;
	}

	public void SetIdentity()
	{
		if (!isLocalPlayer)
			playerTransform.name = playerIdentity;
		else
			playerTransform.name = MakeUniqueIdentity();
	}

	[Client]
	public void GetNetworkIdentity()
	{
		playerNetID = GetComponent<NetworkIdentity>().netId;
		CmdTellServerPlayerIdentity(MakeUniqueIdentity());
	}

	[Command]
	public void CmdTellServerPlayerIdentity(string identity)
	{
		playerIdentity = identity;
	}
}
