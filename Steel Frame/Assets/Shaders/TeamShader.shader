﻿Shader "TeamShader" {
Properties {
	_TeamColor("Team Color", Color) = (1,1,1,1)
	_ColorSwap ("Color to Swaop", Color) = (1,1,1,1)
	_SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
	_Shininess ("Shininess", Range (0.03, 1)) = 0.078125
	_MainTex ("Base (RGB) Gloss (A)", 2D) = "white" {}
	_BumpMap ("Normalmap", 2D) = "bump" {}
}

CGINCLUDE
sampler2D _MainTex;
sampler2D _BumpMap;
fixed4 _TeamColor;
fixed4 _ColorSwap;
half _Shininess;

struct Input {
	float2 uv_MainTex;
	float2 uv_BumpMap;
};

void surf (Input IN, inout SurfaceOutput o) {
	half4 tex = tex2D(_MainTex, IN.uv_MainTex);

	if (all(_ColorSwap.rgb == tex.rgb))
		tex.rgb = _TeamColor;

	o.Albedo = tex.rgb;
	o.Gloss = tex.a;
	o.Alpha = tex.a;
	o.Specular = _Shininess;
	o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
}
ENDCG

SubShader { 
	Tags { "RenderType"="Opaque" }
	LOD 400
	
	CGPROGRAM
	#pragma surface surf BlinnPhong
	#pragma target 3.0
	ENDCG
}

SubShader { 
	Tags { "RenderType"="Opaque" }
	LOD 400
	
	CGPROGRAM
	#pragma surface surf BlinnPhong nodynlightmap
	ENDCG
}

FallBack "Legacy Shaders/Specular"
}
