﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class NetworkPlayerAttributes : NetworkBehaviour
{
	[SyncVar(hook = "OnHealthChange")] public float health;

	public void TakeDamage(float damage)
	{
		health -= damage;
	}

	public void OnHealthChange(float current)
	{
		health = current;
	}
}
