﻿using UnityEngine;
using System.Collections;

public class CameraSwapper : MonoBehaviour
{
    public Transform fp_target;
    public Transform tp_target;
    public float swap_speed = 1.0f;
    public float maxPitch;
    public float sensitivity;

    private bool swapping = false;
    private bool fp_cam = true;
    private float total_pitch = 0.0f;

    public void LateUpdate()
    {
        SwitchViews();
        Inputs();
    }

    public void SwitchViews()
    {
        if (Input.GetButtonDown("SwapCamera") && !swapping)
        {
            swapping = true;
            fp_cam = !fp_cam;
            StartCoroutine("Swap");
        }
    }

    IEnumerator Swap()
    {
        float elapsedTime = 0;

        while (elapsedTime < 1)
        {
            yield return null;

            elapsedTime += Time.deltaTime * swap_speed;

            if (fp_cam) //Lerp from Third person to First person
            {
                transform.position = Vector3.Lerp(tp_target.position, fp_target.position, elapsedTime);
                transform.rotation = Quaternion.Lerp(tp_target.rotation, fp_target.rotation, elapsedTime);
            }
            else //Lerp from First person to Third person
            {
                transform.position = Vector3.Lerp(fp_target.position, tp_target.position, elapsedTime);
                transform.rotation = Quaternion.Lerp(fp_target.rotation, tp_target.rotation, elapsedTime);
            }
        }

        if (fp_cam)
        {
            transform.position = Vector3.Lerp(tp_target.position, fp_target.position, 1.0f);
            transform.rotation = Quaternion.Lerp(tp_target.rotation, fp_target.rotation, 1.0f);
        }
        else
        {
            transform.position = Vector3.Lerp(fp_target.position, tp_target.position, 1.0f);
            transform.rotation = Quaternion.Lerp(fp_target.rotation, tp_target.rotation, 1.0f);
        }

        swapping = false;

        total_pitch = 0.0f;
    }

    public void Inputs()
    {
        if (!swapping)
        {
            float angle = Input.GetAxis("Mouse Y") * sensitivity;

            if (total_pitch > -maxPitch && total_pitch < maxPitch)
            {
                total_pitch += angle;

                if (fp_cam)
                    transform.Rotate(Vector3.right, -angle);
                else
                    transform.RotateAround(fp_target.position, transform.right, -angle);
            }
            if (total_pitch <= -maxPitch || total_pitch >= maxPitch)
            {
                total_pitch -= angle;

                if (fp_cam)
                    transform.Rotate(Vector3.right, angle);
                else
                    transform.RotateAround(fp_target.position, transform.right, angle);
            }
        }
    }
}
