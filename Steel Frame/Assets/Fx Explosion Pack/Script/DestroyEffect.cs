﻿using UnityEngine;
using System.Collections;

public class DestroyEffect : MonoBehaviour
{
    float timer;
    public float lifetime = 2.0f;

    void Start()
    {
        timer = lifetime;
    }

	void Update ()
	{
        timer -= Time.deltaTime;

        if (timer <= 0)
            Destroy(gameObject);
	}
}
