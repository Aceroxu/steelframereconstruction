﻿Shader "Custom/DepletableBar"
{
	Properties
	{
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_FillAmount ("Percent of Bar Filled", Range(0.0, 1.0)) = 1.0
	}
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		Pass
		{
			CGPROGRAM
			
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			sampler2D _MainTex;
			float _FillAmount;
			
			struct v2f {
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
			};
			
			float4 _MainTex_ST;
			
			v2f vert (appdata_base v)
			{
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
				return o;
			}
			
			fixed4 frag (v2f i) : COLOR
			{
				if (i.uv.x >= _FillAmount)
					discard;
				fixed4 c = tex2D(_MainTex, i.uv);
				return c;
			}
			
			
			ENDCG
		}
	} 
	FallBack "Diffuse"
}
