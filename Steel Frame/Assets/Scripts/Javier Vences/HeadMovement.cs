﻿using UnityEngine;
using System.Collections;

public class HeadMovement : MonoBehaviour 
{
    public float m_speed = 1.0f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
        float horizontal = Input.GetAxis("Horizontal");

        transform.Rotate(Vector3.up, horizontal * Time.deltaTime * m_speed);
	}
}
