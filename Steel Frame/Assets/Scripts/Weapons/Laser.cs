﻿using UnityEngine;
using System.Collections;

public class Laser : MonoBehaviour
{
    enum STATE { NORMAL, OVERHEAT }

    public Vector3 offset;

    public float fireRate = .5f; // Shots per second
    public float timeSinceLastShot;
    public float heatLevel; // Current heat value
    public float maxHeat = 100f; // Maximum heat value, overheat when exceeded
    public float coolingRate = 3; // Reduce heat level by this per second
    public float heatPerShot = 7f;
    //public float fireRate;

    public GameObject laserPrefab;

    int state;

    void Start()
    {
        heatLevel = 0f;
        timeSinceLastShot = fireRate;
        state = (int)STATE.NORMAL;


    }

    // Update is called once per frame
    void FixedUpdate ()
    {
        timeSinceLastShot += Time.deltaTime;

        if (heatLevel <= 0f)
        {
            heatLevel = 0;
            if (state == (int)STATE.OVERHEAT)
                state = (int)STATE.NORMAL;
        }
        else if (heatLevel >= maxHeat && state != (int)STATE.OVERHEAT)
        {
            state = (int)STATE.OVERHEAT;
        }
        else
            coolWeapon();

        if(Input.GetAxis("Fire") > .01f && state != (int)STATE.OVERHEAT)
        {
            if(timeSinceLastShot >= fireRate)
            {
                fireOneShot();
            }
        }
	}

    void fireOneShot()
    {
        //ProjectileManager.PM.GetLaser(gameObject.transform.position, gameObject.transform.rotation);
        GameObject go = (GameObject)Instantiate(laserPrefab, gameObject.transform.position + offset, gameObject.transform.rotation);
        heatLevel += heatPerShot;

        timeSinceLastShot = 0f;
    }

    void coolWeapon()
    {
        heatLevel -= coolingRate * Time.deltaTime;
    }

    public float getCurrentHeat()
    {
        return heatLevel;
    }

    public float getMaxHeat()
    {
        return maxHeat;
    }

    public int getState()
    {
        return (int)state;
    }
}
