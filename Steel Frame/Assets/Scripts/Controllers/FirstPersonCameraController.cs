﻿using UnityEngine;
using System.Collections;

public class FirstPersonCameraController : MonoBehaviour
{

    public float sensitivityX = 15F;
    public float sensitivityY = 15F;

    public float minimumX = -360F;
    public float maximumX = 360F;

    public float minimumY = -60F;
    public float maximumY = 60F;

    float rotationY = 0F;
    float rotationX = 0F;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        rotationX += Input.GetAxis("Mouse X") * sensitivityX;
        rotationX = Mathf.Clamp(rotationX, minimumX, maximumX);

        rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
        rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

        //transform.parent.transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
        transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);

        //transform.position = new Vector3(0f, 0f, 0f);
    }
}