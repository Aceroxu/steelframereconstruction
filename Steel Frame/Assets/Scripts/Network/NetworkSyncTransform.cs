﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class NetworkSyncTransform : NetworkBehaviour
{
	public Transform playerTransform;
	public float lerp;
	public float positionThreshold;
	public float rotationThreshold;

	[SyncVar] public Vector3 playerPosition;
	public Vector3 lastPosition;
    [SyncVar] public Quaternion playerRotation;
	public Quaternion lastRotation;


	public void Update()
	{
        NetworkPlayerUpdate();
	}

    public void NetworkPlayerUpdate()
    {
        TransmitPlayerTransform();
		LerpPlayerPosition();
		LerpPlayerRotation();
    }

    public void LerpPlayerPosition()
    {
        if (!isLocalPlayer)
            playerTransform.position = Vector3.Lerp(playerTransform.position, playerPosition, (lerp * Time.deltaTime));
    }

	public void LerpPlayerRotation()
    {
        if (!isLocalPlayer)
			playerTransform.rotation = Quaternion.Lerp(playerTransform.rotation, playerRotation, (lerp * Time.deltaTime));
    }

	[Command]
    public void CmdProvidePositionToServer(Vector3 position)
	{
		playerPosition = position;
	}

	[Command]
	public void CmdProvideRotationToServer(Quaternion rotation)
	{
		playerRotation = rotation;
	}
    
    [ClientCallback]
    public void TransmitPlayerTransform()
    {
		if (isLocalPlayer)
		{
			if (Vector3.Distance(playerTransform.position, lastPosition) > positionThreshold)
			{
				CmdProvidePositionToServer(playerTransform.position);
				lastPosition= playerTransform.position;
			}
			if (Quaternion.Angle(playerTransform.rotation, lastRotation) > rotationThreshold)
			{
				CmdProvideRotationToServer(playerTransform.rotation);
				lastRotation = playerTransform.rotation;
			}
		}
    }
}
