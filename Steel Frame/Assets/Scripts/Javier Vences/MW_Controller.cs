﻿using UnityEngine;
using System.Collections;

public class MW_Controller : MonoBehaviour 
{
	//top speed mech can reach by pressing forward
	public float m_maxSpeed;

	//how fast the mech accelerates when pressing forward
	public float m_acceleration;

	//how fast the mechs legs will rotate
	public float m_rotationSpeed;

	//how maximum rotation speed
	public float m_maxRotationSpeed = 1;
	
	private Vector3 m_velocity;
	private Rigidbody m_rigidBody;	

	void Start()
	{
		m_velocity = Vector3.zero;
		m_rigidBody = GetComponent<Rigidbody> ();
		m_rigidBody.maxAngularVelocity = m_maxRotationSpeed;
	}

	void Update()
	{
		MoveMech ();
		RotateMech ();
	}

	void MoveMech()
	{
		float moveDir = Input.GetAxis ("Vertical");

		//Get currently velocity
		m_velocity = m_rigidBody.velocity;

		//Get current speed
		float speedSqr = m_velocity.sqrMagnitude;

		if (moveDir != 0.0f) 
		{
			//If current speed is less than max speed, then apply acceleration
			if(speedSqr < m_maxSpeed * m_maxSpeed)
			{
				m_velocity += transform.forward * moveDir * m_acceleration * Time.deltaTime;
			}
		}

		m_rigidBody.velocity = m_velocity;
	}

	void RotateMech()
	{
		float rotDir = Input.GetAxis ("Horizontal");

		if(rotDir != 0)
		{
			m_rigidBody.AddTorque(Vector3.up * rotDir * m_rotationSpeed * Time.deltaTime);
		}
	}
}































