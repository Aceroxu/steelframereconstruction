﻿using UnityEngine;
using System.Collections;

public class MechStatus : MonoBehaviour
{
    // Current health and maximum health
    public int hp;
    public int maxHp = 100;

    public GameObject explosion;
    public MeshRenderer spawnScreen;

	void Start ()
    {
        hp = maxHp;
	}

	void Update ()
    {
	    
	}

    public void takeDamage(int damage)
    {
        hp -= damage;
        if (hp <= 0)
            death();
    }

    void death()
    {
        Debug.Log("I have died!");

        GameObject go = (GameObject)Instantiate(explosion, transform.position, transform.rotation);

        // Move mech out of the way
        Vector3 moveTo = new Vector3(-900, -900, -900);
        
        StartCoroutine("respawn");
    }

    IEnumerator respawn()
    {
        float timer = 0.0f;
        spawnScreen.enabled = true;


        while(timer < 3.0f)
        {
            timer += Time.deltaTime;
            yield return null;
        }

        spawnScreen.enabled = false;

        Spawn();
    }

    void Spawn()
    {
        hp = maxHp;
        // Get spawn position and move there


        // Temporarily randomly choose numbers
        Vector3 spawnPos;
        spawnPos.x = Random.Range(-5f, 5f);
        spawnPos.z = Random.Range(-5f, 5f);
        spawnPos.y = 2.0f;
    }
}