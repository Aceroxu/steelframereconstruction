﻿/*
 *  Written by Reuben R. Karpovich <linuxreuben@gmail.com>
 *  
 *  @NOTE: This class should check to make sure that the size of sounds[] and playing are the same[]
 *  @TODO: Make into a static class, add code that does the above
 */
 #define TEST

using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

    public AudioClip[] sounds;
    public enum PlayType {PLAY, PLAYIFNOTPLAYING, KILLANDPLAY};
    private SoundManager singleton;

	#if TEST
	public bool testPlaySound;
	public PlayType testPlayType;
	public uint testSoundSelect;
	#endif

    public SoundManager instance
    {
        get
        {
            if (singleton == null)
            {
                singleton = GameObject.FindObjectOfType<SoundManager>();
                if (singleton == null)
                {
                    GameObject managers = GameObject.Find("Managers");
                    if (managers == null)
                    {
                        managers = new GameObject("Managers");
                        singleton = managers.AddComponent<SoundManager>();
                    }
                }
            }
            return singleton;
        }
    }

    // Use this for initialization
    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    void Start () {	
	}
	
	// Update is called once per frame
	void Update () {
		#if TEST
		TestPlaySound();
		#endif
    }

    // Uses an int to decide what sound to play
    // Plays sound regardless of if it's playing already
    public void Play(uint index)
    {
        Play(index, PlayType.PLAY);
    }

    // Uses an int to decide what sound to play
    // Here you can choose how it will handle the play request
    public void Play(uint index, PlayType playType)
    {
		GetComponent<AudioSource>().clip = sounds[index];
		if (playType == PlayType.PLAY)
		{
			GetComponent<AudioSource>().Play();
		}
    }

    // Uses a string to decide what sound to play
    // Plays sound regardless of if it's playing already
    public void Play(string index)
    {
        Play(index, PlayType.PLAY);
    }

    // Uses a string to decide what sound to play
    // Here you can choose how it will handle the play request
    public void Play(string index, PlayType playType)
    {
		for (int i = 0; i < sounds.Length; i++)
		{
			if (sounds[i].name == index)
				GetComponent<AudioSource>().clip = sounds[i];
		}

		if (playType == PlayType.PLAY)
		{
			GetComponent<AudioSource>().Play();
		}
	}

	#if TEST
	void TestPlaySound()
	{
		if (testPlaySound)
		{
			Play(testSoundSelect, testPlayType);
			testPlaySound = false;
		}
	}
	#endif
}
