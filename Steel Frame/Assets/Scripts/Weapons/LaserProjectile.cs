﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class LaserProjectile : NetworkBehaviour
{
    public float speed;
	public float damage;
    float lifetime;
    float destroyAfterSeconds = 2f;

    void Start()
    {
        lifetime = 0f;
    }

	// Update is called once per frame
	void FixedUpdate ()
    {
		if(!isLocalPlayer)
			return;

        Vector3 dir = gameObject.transform.forward;
        dir = Vector3.Normalize(dir);
        Vector3 newPos = gameObject.transform.position + (dir * speed);
        gameObject.transform.position = newPos;

        lifetime += Time.deltaTime;
        if (lifetime >= destroyAfterSeconds)
            Destroy(gameObject);
	}

    void OnCollisionEnter(Collision col)
	{
		if (!isLocalPlayer)
			return;
		string uidentity = col.transform.name;
		CmdTransmitWhoWasHit(uidentity, damage);
        if(col.gameObject.tag == "Player")
        {
            col.gameObject.GetComponent<MechStatus>().takeDamage(10);
        }

        Destroy(gameObject);
    }

	[Command]
	public void CmdTransmitWhoWasHit(string uidentity, float damage)
	{
		GameObject go = GameObject.Find(uidentity);
	}
}
