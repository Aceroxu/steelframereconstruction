﻿using UnityEngine;
using System.Collections;

public class PlayerController : CharacterControls
{
	private bool fire;
	private float mouse;
    private float horizontal;
    private float vertical;

	public Transform[] projectileSpawnpoints;

	public void Update()
    {
        PlayerInputs();
		PelvisRotation(horizontal);
		Movement(vertical);
		TorsoRotation(mouse);
    }

    public void PlayerInputs()
    {
		fire = Input.GetKeyDown(KeyCode.Mouse0);
        mouse = Input.GetAxis("Mouse X");
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");
        jumpjet = (Input.GetAxis ("Jump") > 0) ? true : false;
    }

	public void Shoot()
	{
		if(!fire)
			return;
		//SpawnManager.instance.CmdShoot(projectileSpawnpoints);
	}
    
    public float GetVertical()
    {
    	return vertical;
    }
    
    public float GetFuel()
    {
    	return jetFuel;
    }
    
    public float GetMaxFuel()
    {
    	return maxFuel;
    }
}
