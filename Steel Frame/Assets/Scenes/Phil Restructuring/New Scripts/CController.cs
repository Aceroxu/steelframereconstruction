﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public struct TransformInfo
{
    TransformInfo(Vector3 v, Quaternion q)
    {
        pos = v;
        rot = q;
    }
    public Vector3 pos;
    public Quaternion rot;
}

public class CController : NetworkBehaviour
{
    [SyncVar]
    TransformInfo tInfo;

    public const float rotationSpeed = .75f;
    public const float forwardAcceleration = 5f;

    public CharacterController controller;

    public Vector3 velocity;
    Vector3 position;

    private Vector3 pelvisRotation;
    private float torsoRotation;
    public const int range = 1;

    public float currentSpeed;
    public const float maxSpeed = 8f;


    public Vector3 acceleration;

    bool isGrounded;

    public Animator animator;

    float hori;
    float vert;
    float mouse;
    bool jumpjet;
    public float jetFuel = 100.0f;
    float maxFuel = 100.0f;
    float refuelRate = 9.0f;
    float drainRate = 70.0f;
    float jumpJetStrength = 3.5f;

    public Vector3 gravity = new Vector3(0.0f,-9.81f,0.0f);
    public float dragMult = .33f;

    [Range(.01f, .1f)]
    public float mouseSensitivity = .015f;
    const float maxVelocity = 40f;

    // Use this for initialization
    void Start ()
    {
        position = transform.position;
        mouseSensitivity = .015f;
        currentSpeed = 0.0f;
    }

// Update is called once per frame
    void FixedUpdate ()
    {
        if (isLocalPlayer)
        {
            getInputs();
        }

        CmdHandleJumpJet();
        CmdGravity();

        CmdPelvisRotation(hori);
        CmdTorsoRotation(mouse);

        CmdhorizontalMovement(vert);
        CmdUpdatePosition();
        SyncTransform();

        CmdDrag();
    }

    void getInputs()
    {
        hori = Input.GetAxis("Horizontal");
        vert = Input.GetAxis("Vertical");
        mouse = Input.GetAxis("Mouse X");
        jumpjet = (Input.GetAxis("Jump") > 0) ? true : false;
    }

    [Command] void CmdHandleJumpJet()
    {
        if (jetFuel < 0)
        {
            jumpjet = false;
            jetFuel = 0.0f;
        }
        else if (jumpjet)
        {
            jetFuel -= drainRate * Time.deltaTime;
        }
        else if (jetFuel < maxFuel)
            jetFuel += refuelRate * Time.deltaTime;
    }

    [Command] void CmdGravity()
    {
        controller.Move(Vector3.down * 0.01f);

        if (jumpjet)
        {
            Debug.Log("JUMPJETS!~");
            acceleration += Vector3.up * jumpJetStrength * Time.deltaTime;
        }
        else if (controller.isGrounded)
        {
            Debug.Log("Decelerate!");
            if(acceleration.y < -0.1f)
            {
                acceleration.y = -0.1f;
            }
            if(velocity.y < -0.1f)
            {
                velocity.y = -0.1f;
            }
        }
        else
        {
            Debug.Log("Falling Down!");
            acceleration += gravity * Time.deltaTime;
        }
    }

    [Command] void CmdUpdatePosition()
    {
        velocity += acceleration * Time.deltaTime;
        if(velocity.magnitude > maxVelocity)
        {
            velocity = velocity.normalized;
            velocity *= maxVelocity;
        }

        controller.Move(velocity * Time.deltaTime);

        tInfo.pos = transform.position;
        tInfo.rot = transform.rotation;
    }

    void SyncTransform()
    {
        transform.position = tInfo.pos;
        transform.rotation = tInfo.rot;
    }

    [Command] public void CmdPelvisRotation(float horizontal)
    {
        pelvisRotation = (horizontal * Vector3.up);
        pelvisRotation *= rotationSpeed;
        transform.Rotate(pelvisRotation);
    }

    [Command] public void CmdTorsoRotation(float mouse)
    {
        torsoRotation += mouse * mouseSensitivity;
        torsoRotation = Mathf.Clamp(torsoRotation, -range, range);
        animator.SetFloat("Rotation", torsoRotation);
    }

    public void Movement(float vertical)
    {
        Vector3 moveDirection = vertical * -Vector3.right;
        moveDirection = transform.TransformDirection(moveDirection.normalized);
        moveDirection *= forwardAcceleration;
        //HandleJumpJet();
        //ApplyGravity();
        animator.SetFloat("Movement", vertical);
        //controller.Move(direction);

        acceleration += moveDirection;// * Time.deltaTime;
    }

    [Command] void CmdDrag()
    {
        if(currentSpeed > 0f)
        {
            currentSpeed -= maxSpeed * dragMult * Time.deltaTime;
        }
        else if(currentSpeed < 0f)
        {
            currentSpeed += maxSpeed * dragMult * Time.deltaTime;
        }
        
    }

    [Command] void CmdhorizontalMovement(float vertical)
    {
        // Adjust speed by acceleration

        if (currentSpeed <= maxSpeed && currentSpeed >= -maxSpeed)
        {
            currentSpeed += forwardAcceleration * Time.deltaTime * vertical;
        }
        else if(currentSpeed > maxSpeed)
        {
            currentSpeed = maxSpeed;
        }
        else
        {
            currentSpeed = -maxSpeed;
        }

        Vector3 moveDirection = -Vector3.right;
        moveDirection = transform.TransformDirection(moveDirection.normalized);

        controller.Move(moveDirection * currentSpeed * Time.deltaTime);
    }
}
