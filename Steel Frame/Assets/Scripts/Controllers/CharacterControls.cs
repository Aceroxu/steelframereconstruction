﻿using UnityEngine;
using System.Collections;

public class CharacterControls : MonoBehaviour
{
	public const float rotationSpeed = .75f;
	public const float movementSpeed = 0.0625f;
	public const int range = 1;

	public CharacterController controller;
	public Animator animator;

	protected bool jumpjet;
	protected float jetFuel = 100.0f;
	protected float maxFuel = 100.0f;
	protected float refuelRate = 9.0f;
	protected float drainRate = 50.0f;
	
	private Vector3 gravity;
	private Vector3 direction;
	private Vector3 pelvisRotation;
	private float torsoRotation;

	[Range(.01f, .1f)] public float mouseSensitivity;
	
	
    // rotates the mechs upper body 
    public void TorsoRotation(float mouse)
    {
		torsoRotation += mouse * mouseSensitivity;
		torsoRotation = Mathf.Clamp(torsoRotation, -range, range);
		animator.SetFloat("Rotation", torsoRotation);
    }

    // rotates the mechs lower body 
    public void PelvisRotation(float horizontal)
    {
		pelvisRotation = (horizontal * Vector3.up);
		pelvisRotation *= rotationSpeed;
		transform.Rotate(pelvisRotation);
    }

	// this will apply the gravity and user input to the object 
	public void Movement(float vertical)
	{
		direction = vertical * -Vector3.right;
		Normalization();
		direction *= movementSpeed;
		HandleJumpJet();
		ApplyGravity();
		animator.SetFloat("Movement", vertical);
		controller.Move(direction);
	}
	
	public void HandleJumpJet()
	{
		if (jetFuel <= 0)
		{
			jumpjet = false;
			jetFuel = 0.0f;
		}
		else if (jumpjet)
		{
			jetFuel -= drainRate * Time.deltaTime;
		}
        if(jetFuel < maxFuel)
            jetFuel += refuelRate * Time.deltaTime;
	}

	// will calculating gravity and apply it to a variable 
	public void ApplyGravity()
	{
		if (jumpjet)
			gravity -= Physics.gravity * Time.deltaTime * 0.01f;
		else if (!controller.isGrounded)
			gravity += Physics.gravity * Time.deltaTime * 0.5f;
		else
			gravity = Vector3.zero;
		direction += gravity;
	}

	public void Normalization()
	{
		direction = direction.normalized;
		direction = transform.TransformDirection(direction);
	}
}
