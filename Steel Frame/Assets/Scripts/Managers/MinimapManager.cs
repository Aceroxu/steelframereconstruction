﻿using UnityEngine;
using System.Collections;

public class MinimapManager : MonoBehaviour
{
	public RenderTexture minimapTexture;
	public Material minimapMaterial;
	
	public Vector2 offset;
	public Vector2 mapSize;
	
	void Update ()
	{
		
	}
	
	void OnGUI ()
	{
		if (Event.current.type == EventType.Repaint)
			Graphics.DrawTexture(new Rect(Screen.width - mapSize.x - offset.x, offset.y, mapSize.x, mapSize.y), minimapTexture, minimapMaterial);
	}
}
