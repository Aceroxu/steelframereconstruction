﻿//Javier Vences

using UnityEngine;
using System.Collections;

public class BaseMovement : MonoBehaviour 
{
	public float m_speed = 2f;
	public float m_rotSpeed = 360f;
	public Transform m_feet;

	private Vector3 moveDirection = Vector3.zero;

	void Update()
	{
		RotateBody ();
		Move ();
	}

	void Move()
	{
		transform.position += moveDirection * m_speed * Time.deltaTime;
	}

	void RotateBody()
	{
		//Get Camera Forward and normalize it
		/*
		Vector3 cameraForward = Camera.main.transform.forward;
		cameraForward.y = 0;
		cameraForward.Normalize ();

		//Get Camera Right
		Vector3 cameraRight = Camera.main.transform.right;
		*/

		moveDirection = Vector3.zero;
		
		float horizontal = Input.GetAxis ("Horizontal");
		float vertical = Input.GetAxis ("Vertical");

		//Apply input to the move direction and normalize it
		if(vertical != 0)
		{
			moveDirection += m_feet.forward.normalized * vertical;
		}
		if(horizontal != 0)
		{
			moveDirection += m_feet.right * horizontal;
		}

		moveDirection.Normalize ();

		//If the player is not pressing keys, don't rotate the feet
		if(horizontal != 0)
		{
			//Quaternion lookTarget = Quaternion.LookRotation((m_feet.transform.position + moveDirection) - transform.position);
            Quaternion lookTarget = Quaternion.LookRotation((m_feet.transform.position + moveDirection) - m_feet.transform.position);
			m_feet.transform.rotation = Quaternion.RotateTowards (m_feet.transform.rotation, lookTarget, m_rotSpeed * Time.deltaTime);
		}
	}
}
