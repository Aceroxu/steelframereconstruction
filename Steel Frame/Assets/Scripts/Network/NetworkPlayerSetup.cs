﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class NetworkPlayerSetup : NetworkBehaviour
{
	[SerializeField] public CharacterController controller;
	[SerializeField] public PlayerController playerController;
	[SerializeField] public Camera camera;
	[SerializeField] public CameraController cameraScript;
	
	[SerializeField] public GameObject PlayerGUI;
	[SerializeField] public GameObject MinimapCamera;
	
	[SerializeField] public Laser weapon1;
	[SerializeField] public Laser weapon2;

	public void Start()
	{
		if (isLocalPlayer)
		{
			controller.enabled = true;
			playerController.enabled = true;
			camera.enabled = true;
			cameraScript.enabled = true;
			
			weapon1.enabled = true;
			weapon2.enabled = true;
			
			GameObject gui = GameObject.Instantiate(PlayerGUI);
			gui.GetComponent<GUIManager>().SetMech(playerController.gameObject);
			gui.GetComponent<GUIManager>().SetMechWeapons(weapon1, weapon2);
			
			GameObject map = GameObject.Instantiate(MinimapCamera);
			map.GetComponent<MinimapFollowTarget>().target = playerController.gameObject.transform;
		}
	}
}
